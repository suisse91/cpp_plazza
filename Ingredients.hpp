//
// Ingredients.hpp for ingredients in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Mon Apr 13 11:50:23 2015 arthur
// Last update Fri Apr 17 16:42:23 2015 raphael elkaim
//

#ifndef INGREDIENT_HPP_
#define INGREDIENT_HPP_

#include <map>
#include <ctime>
#include <string>

class Ingredient
{
  static Ingredient		*ig;
  std::map<std::string, int>	nbrs;
  time_t			time;
  int				rTime;

  Ingredient();
  ~Ingredient();

public:
  static Ingredient *Instance()
  {
    if (!ig)
      {
	ig = new Ingredient;
	return ig;
      }
    return ig;
  }

  bool	pick(const std::string &);
  bool	tryPick(const std::string &);
  void	setReplaceTime(int);
  void	update();
};

#endif
