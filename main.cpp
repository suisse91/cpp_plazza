//
// main.cpp for main in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr  8 17:51:10 2015 arthur
// Last update Thu Apr  9 11:58:04 2015 arthur
//

#include <iostream>
#include <unistd.h>
#include "Reception.hpp"

int	main(int ac, char **av)
{
  float	mul;
  int	nbCooks, rTime;
  
  if (ac != 4)
    {
      std::cout << "Usage: ./plazza [multiplier] [cooks per kitchen] [replace time]" << std::endl;
      return false;
    }
  
  mul = atof(av[1]);
  nbCooks = atoi(av[2]);
  rTime = atoi(av[3]);
  if (mul <= 0 || nbCooks <= 0 || rTime <= 0)
    {
      std::cout << "La pizzeria est fermée pour cause de fdputerie" << std::endl;
      return false;
    }
  Reception	r(mul, nbCooks, rTime);
  r.start();
  
  return true;
}
