//
// process.hpp for process in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 13:38:56 2015 raphael elkaim
// Last update Fri Apr 17 15:18:56 2015 raphael elkaim
//

#ifndef PROCESS_HPP_
# define PROCESS_HPP_

#include <cstdlib>
#include "plazza.hpp"
#include "Fifo.hpp"
#include "PipeOut.hpp"
#include "PipeIn.hpp"
#include "Kitchen.hpp"

template<class T>
class Process
{
  T		*obj;
  Fifo		pipeF;
  Fifo		pipeC;
  //  InPipe	inn;
  //OutPipe	oout;
  int		child;

public:
  Process(int idx):
    pipeF((std::string("/tmp/ParentFifoKitchen") + idx)),
    pipeC((std::string("/tmp/ChildFifoKitchen") + idx)),
    child(0)
  {
  }

  ~Process()
  {
  }

  int	fork(int nbCook, float mult, int replace)
  {
    int	i(0);

    i = ::fork();
    if (i == -1)
      {
	return -1;
      }
    if (!i)
      {
	child = 1;
	obj = new T(nbCook, mult, replace, this);
	pipeF.openRd();
	pipeC.openWr();
	obj->run();
	delete obj;
	return -2;
      }
    else
      {
	pipeF.openWr();
	pipeC.openRd();
      }
    return 0;
  }

  Fifo &in()
  {
    if (child)
      return pipeF;
    return pipeC;
  }

  Fifo &out()
  {
    if (!child)
      return pipeF;
    return pipeC;
  }

  /*  bool	isRunning() const
  {
    return obj->isAvailable();
  }

  bool	isAvailable() const
  {
    return obj->isAvailable();
    }*/
  
};

#endif
