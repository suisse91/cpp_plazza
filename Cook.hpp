//
// Thread.hpp for thread in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:15:07 2015 arthur
// Last update Thu Apr 23 15:50:02 2015 raphael elkaim
//

#ifndef COOK_HPP_
#define COOK_HPP_

#include <map>
#include <vector>
#include <string>
#include "plazza.hpp"
#include "CondVar.hpp"
#include "Ingredients.hpp"
#include "Kitchen.hpp"
#include "Mutex.hpp"

class Kitchen;

class Cook
{
  float							multiplier;
  static std::map<TypePizza, int>			cTime;
  static std::map<TypePizza, std::vector<std::string> >	pizzas;
  Kitchen						*parent;
  CondVar<Ingredient*>					*ingredients;

public:
  Cook(Kitchen *, float);
  ~Cook();
  
  bool	cookPizza(TypePizza) const;
  bool	checkIngredients(TypePizza, bool test = true) const;
  void	run();
private:
  static std::map<TypePizza, int>			initTimes();
  static std::map<TypePizza, std::vector<std::string> >	initPizzas();
};

#endif
