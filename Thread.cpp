//
// Thread.cpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Thu Apr 23 12:09:25 2015 raphael elkaim
// Last update Thu Apr 23 16:13:29 2015 raphael elkaim
//

#include "Thread.hpp"

Thread::Thread(thr fct):
  function(fct)
{
}

Thread::~Thread()
{
}

void	Thread::start(void *arg)
{
  pthread_create(&th, NULL, function, arg);
}

void	Thread::join()
{
  void	*ret;

  pthread_join(th, &ret);
}

void Thread::yield()
{
  pthread_yield();
}
