//
// fifo.cpp for plaza in /home/elkaim_r/projects/cpp/cpp_plazza
// 
// Made by elkaim raphael
// Login   <elkaim_r@epitech.net>
// 
// Started on  Thu Apr 16 12:58:01 2015 elkaim raphael
// Last update Fri Apr 17 12:19:26 2015 raphael elkaim
//
 
#include "Fifo.hpp"

Fifo::Fifo(const std::string &fname) :
  fi(fname),
  in(fname),
  out(fname)
{
  valid = mkfifo(fname.c_str(), 0666);
}

const std::string &Fifo::name() const
{
  return fi;
}

bool	Fifo::operator!()
{
  return valid? false : true;
}

void Fifo::openRd()
{
  in.open();
}

void Fifo::openWr()
{
  out.open();
}

Fifo &Fifo::operator>>(std::string &str)
{
  in >> str;
  return *this;
}

bool Fifo::changed()
{
  return in.changed();
}

Fifo &Fifo::operator<<(const std::string &str)
{
  out << str;
  return *this;
}

Fifo::~Fifo()
{
  unlink(fi.c_str());
}
