//
// pipeOut.cpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 15:03:49 2015 raphael elkaim
// Last update Fri Apr 17 11:53:34 2015 raphael elkaim
//

#include "PipeOut.hpp"


OutPipe::OutPipe(const std::string &fname) :
  Pipe(fname),
  fifo()
{
  /*if (!fifo)
    {
      std::cout << "YOU HAVE FAILED ME OUT" << std::endl;
      isValid = 0;
      return ;
      }*/
}

OutPipe::~OutPipe()
{
  fifo.close();
}

bool	OutPipe::open()
{
  fifo.open(fifoName.c_str(), std::ios_base::out);
  if (!fifo)
    return false;
  return true;
}


OutPipe &OutPipe::operator<<(const std::string &str)
{
  fifo << str << std::endl;
  return *this;
}
