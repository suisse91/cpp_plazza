//
// Kitchen.hpp for kitchen in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr  8 18:24:32 2015 arthur
// Last update Thu Apr 23 16:02:37 2015 raphael elkaim
//

#ifndef KITCHEN_HPP_
#define KITCHEN_HPP_

#include <ctime>
#include <queue>
#include "CondVar.hpp"
#include "Process.hpp"
#include "Ingredients.hpp"
#include "Cook.hpp"
#include "Thread.hpp"
#include "Reception.hpp"

class Kitchen
{
  typedef void (Kitchen::*fnc)(std::string);
  int				nbCooks;
  int				nbPizzas;
  float				multiplier;
  int				replaceTime;
  time_t			time;
  Process<Kitchen>		*proc;
  CondVar<Ingredient*>		*ingredients;
  bool				isOpen;
  std::map<std::string, fnc>	funcs;
  CondVar<std::queue<std::string> *> commands;
  Mutex				pizzaLock;
  int				pizza;

  void	checkAlive(std::string);
  void	commandPizza(std::string);
  void	checkAvail(std::string);

public:
  Kitchen(int, float, int, Process<Kitchen>*);
  ~Kitchen();

  void	run();
  void	decr();
  CondVar<Ingredient*>		*ing();
  float				mult();
  const std::string getLastPizza(bool);
};

#endif
