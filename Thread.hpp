//
// Thread.hpp for thread in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:15:07 2015 arthur
// Last update Thu Apr 23 15:12:37 2015 raphael elkaim
//

#ifndef THREAD_HPP_
#define THREAD_HPP_

#include <pthread.h>

typedef void *(*thr)(void *);

/*  template <typename T>*/
class Thread
{
  pthread_t	th;
  thr		function;

public:
  Thread(thr fct);
  ~Thread();
  void	start(void *);
  void	join();
  static void yield();
};

#endif
