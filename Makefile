##
## Makefile for plazza in /home/amstuta/rendu/cpp_plazza
##
## Made by arthur
## Login   <amstuta@epitech.net>
##
## Started on  Wed Apr  8 17:46:14 2015 arthur
## Last update Thu Apr 23 12:14:08 2015 raphael elkaim
##

RM	= rm -f

CC	= g++

CPPFLAGS= -Wall -Wextra -Werror

NAME	= plazza

SRCS	= main.cpp \
	  Reception.cpp \
	  Pipe.cpp \
	  PipeOut.cpp \
	  PipeIn.cpp \
	  Ingredients.cpp \
	  Kitchen.cpp \
	  Mutex.cpp \
	  Cook.cpp \
	  Fifo.cpp \
	  plazza.cpp \
	  Thread.cpp

OBJS	= $(SRCS:.cpp=.o)

all:	$(NAME)

$(NAME):$(OBJS)
	$(CC) $(OBJS) -o $(NAME) -lpthread

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
