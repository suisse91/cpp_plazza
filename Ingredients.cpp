//
// Ingredients.cpp for ingredients in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Mon Apr 13 11:56:45 2015 arthur
// Last update Mon Apr 13 16:18:35 2015 raphael elkaim
//

#include "Ingredients.hpp"

Ingredient *Ingredient::ig = 0;

Ingredient::Ingredient():
  nbrs(),
  time(::time(0))
{
  nbrs["doe"] = 5;
  nbrs["tomato"] = 5;
  nbrs["gruyere"] = 5;
  nbrs["ham"] = 5;
  nbrs["mushrooms"] = 5;
  nbrs["steak"] = 5;
  nbrs["eggplant"] = 5;
  nbrs["goat cheese"] = 5;
  nbrs["chief love"] = 5;
}

Ingredient::~Ingredient()
{
}

bool		Ingredient::tryPick(const std::string &i)
{ 
  if (nbrs.find(i) != nbrs.end() && nbrs[i] > 0)
    return true;
  return false;
}

bool		Ingredient::pick(const std::string &i)
{
  if (nbrs.find(i) != nbrs.end() && nbrs[i] > 0)
    {
      nbrs[i] -= 1;
      return true;
    }
  return false;
}

void		Ingredient::setReplaceTime(int replaceTime)
{
  rTime = replaceTime;
}

void		Ingredient::update()
{
  time_t	diff;
  int		plus;

  diff = ::time(0) - time;
  plus = diff / rTime;
  for (std::map<std::string, int>::iterator it=nbrs.begin(); it != nbrs.end(); ++it)
    it->second += plus;
  time = ::time(0);
}
