//
// plazza.hpp for plazza in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Thu Apr  9 15:06:01 2015 arthur
// Last update Thu Apr 23 14:09:28 2015 raphael elkaim
//

#ifndef PLAZZA_HPP_
#define PLAZZA_HPP_

#include <string>

enum TypePizza
 {
   Regina = 1,
   Margarita = 2,
   Americaine = 4,
   Fantasia = 8
 };

enum TaillePizza
  {
    S = 1,
    M = 2,
    L = 4,
    XL = 8,
    XXL = 16
  };

std::string operator+(std::string , int);
void		     *threadedCook(void *);

#endif
