//
// pipe.hpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 13:56:46 2015 raphael elkaim
// Last update Thu Apr 16 15:51:25 2015 elkaim raphael
//

#ifndef PIPEO_HPP_
# define PIPEO_HPP_

#include "Pipe.hpp"

class OutPipe : public Pipe
{
public:

  OutPipe(const std::string &);
  ~OutPipe();
  bool	open();
  OutPipe &operator<<(const std::string &);


protected:
  std::fstream fifo;
};

#endif
