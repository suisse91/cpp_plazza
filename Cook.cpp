//
// Thread.hpp for thread in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:15:07 2015 arthur
// Last update Thu Apr 23 15:52:25 2015 raphael elkaim
//

#include <unistd.h>
#include "Cook.hpp"

std::map<TypePizza, int> Cook::cTime = initTimes();
std::map<TypePizza, std::vector<std::string> > Cook::pizzas = initPizzas();


std::map<TypePizza, std::vector<std::string> > Cook::initPizzas()
{
  std::map<TypePizza, std::vector<std::string> >	res;

  res[Margarita].push_back("doe");
  res[Margarita].push_back("tomato");
  res[Margarita].push_back("gruyere");

  res[Regina].push_back("doe");
  res[Regina].push_back("tomato");
  res[Regina].push_back("gruyere");
  res[Regina].push_back("ham");
  res[Regina].push_back("mushrooms");

  res[Americaine].push_back("doe");
  res[Americaine].push_back("tomato");
  res[Americaine].push_back("gruyere");
  res[Americaine].push_back("steak");

  res[Fantasia].push_back("doe");
  res[Fantasia].push_back("tomato");
  res[Fantasia].push_back("eggplant");
  res[Fantasia].push_back("goat cheese");
  res[Fantasia].push_back("chief love");

  return res;
}

std::map<TypePizza, int>	Cook::initTimes()
{
  std::map<TypePizza, int>	res;

  res[Margarita] = 1;
  res[Regina] = 2;
  res[Americaine] = 2;
  res[Fantasia] = 4;

  return res;
}

Cook::Cook(Kitchen *par, float mul):
  multiplier(mul),
  parent(par),
  ingredients(par->ing())
{
}

Cook::~Cook()
{
}

void Cook::run()
{
  while (1)
    {
      if (parent->getLastPizza(false) != "")
	{
	  cookPizza(Reception::pizzas[parent->getLastPizza(true)]);
	}
      usleep(50000);
    }
}

bool	Cook::checkIngredients(TypePizza type, bool test) const
{
  for (unsigned int i=0; i < pizzas[type].size(); ++i)
    {
      if (!ingredients->getVal()->Instance()->tryPick(pizzas[type][i]))
	return false;
      if (!test)
	ingredients->getVal()->Instance()->pick(pizzas[type][i]);
    }
  return true;
}

bool	Cook::cookPizza(TypePizza type) const
{
  float	cookingTime = cTime[type] * multiplier * 1000;
  
  if (!checkIngredients(type))
    return false;
  usleep(cookingTime);
  return true;
}
