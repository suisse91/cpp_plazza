//
// Kitchen.cpp for kitchen in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr  8 18:40:45 2015 arthur
// Last update Thu Apr 23 16:02:05 2015 raphael elkaim
//

#include <unistd.h>
#include "Kitchen.hpp"

Kitchen::Kitchen(int nbCooks, float multiplier, int replaceTime, Process<Kitchen> *p):
  nbCooks(nbCooks),
  nbPizzas(0),
  multiplier(multiplier),
  replaceTime(replaceTime),
  time(::time(0)),
  proc(p),
  isOpen(true),
  funcs(),
  commands(new std::queue <std::string>),
  pizza(0)
{
  funcs["CHK"] = &Kitchen::checkAlive;
  funcs["CMD"] = &Kitchen::commandPizza;
  funcs["AVL"] = &Kitchen::checkAvail;
  for (int i(0); i < nbCooks; i++)
    {
      Thread *cooker = new Thread(&threadedCook);
      cooker->start(this);
    }
}

Kitchen::~Kitchen()
{
}

void	Kitchen::checkAlive(std::string cmd)
{
  (void)cmd;
  if (::time(0) - time > 5)
    {
      proc->out() << "LFT";
      std::exit(0);
    }
  proc->out() << "HERE";
}

void	Kitchen::checkAvail(std::string cmd)
{
  (void)cmd;
  Cook cooker(this, multiplier);
  std::string kind = cmd.substr(cmd.find_first_of(" "));
  if (pizza > nbCooks * 2 || cooker.checkIngredients(Reception::pizzas[kind]) == false)
    proc->out() << "NOK";
}

void	Kitchen::commandPizza(std::string cmd)
{
  std::string kind = cmd.substr(cmd.find_first_of(" "));
  commands.getVal()->push(kind);
  commands.unlock();
  pizzaLock.lock();
  pizza++;
  pizzaLock.unlock();
}

void	Kitchen::run()
{
  std::string	res;
  fnc		call;

  while (true)
    {
      proc->in() >> res;
      if (res.empty() == false)
	{
	  std::cout << res << std::endl;
	  std::string com(res.substr(0, res.find_first_of(" ")));
	  if (funcs.find(com) != funcs.end())
	    {
	      call = funcs[com];
	      (this->*call)(res);
	    }
	}
      else
	break ;
      usleep(500);
    }
  isOpen = false;
  std::cout << "SEE YA" << std::endl;
}

const std::string Kitchen::getLastPizza(bool test)
{
  std::string value;
  value = commands.getVal()->front();
  if (test)
    {
      commands.unlock();
      return value;
    }
  commands.getVal()->pop();
  commands.unlock();
  return value;
}

void	Kitchen::decr()
{
  pizzaLock.lock();
  pizza--;
  pizzaLock.unlock();
}

CondVar<Ingredient*>		*Kitchen::ing()
{
  return ingredients;
}

float		Kitchen::mult()
{
  return multiplier;
}
