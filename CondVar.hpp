//
// CondVar.hpp for condvar in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:57:59 2015 arthur
// Last update Thu Apr 23 15:02:13 2015 raphael elkaim
//

#ifndef CONDVAR_HPP_
#define CONDVAR_HPP_

#include "Mutex.hpp"

template <typename T>
class CondVar
{
  T			value;
  Mutex			locker;

public:
  
  CondVar(T val):
    value(val)
  {
  }

  ~CondVar() {}

  T	&getVal()
  {
    locker.lock();
    return value;
  }

  void	unlock()
  {
    locker.unlock();
  }

  void	setVal(T val)
  {
    locker.lock();
    value = val;
    locker.unlock();
  }
  
};

#endif
