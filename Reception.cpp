//
// Reception.cpp for reception in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Thu Apr  9 11:51:01 2015 arthur
// Last update Fri Apr 17 15:55:12 2015 raphael elkaim
//

#include <sstream>
#include <iostream>
#include "Reception.hpp"

Reception::Reception(float mul, int nbCooks, int rpTime):
  multiplier(mul),
  nbCooks(nbCooks),
  replaceTime(rpTime)
{
  pizzaSizes["S"] = S;
  pizzaSizes["M"] = M;
  pizzaSizes["L"] = L;
  pizzaSizes["XL"] = XL;
  pizzaSizes["XXL"] = XXL;
}

Reception::~Reception()
{
}

void		Reception::start()
{
  std::string	buffer;
  
  while (true)
    {
      getline(std::cin, buffer);
      treatCmdLine(buffer);
    }
}

void		Reception::split(const std::string& s, char c, std::vector<std::string>& v)
{
  std::string::size_type i = 0;
  std::string::size_type j = s.find(c);

  if (j == std::string::npos)
    {
      v.push_back(s);
      return;
    }
  while (j != std::string::npos)
    {
      v.push_back(s.substr(i, j-i));
      i = ++j;
      j = s.find(c, j);
      if (j == std::string::npos && !s.substr(i, s.length()).empty())
	v.push_back(s.substr(i, s.length()));
    }
}

void		Reception::treatCmdLine(std::string &buffer)
{
  std::vector<std::string>	splite;

  split(buffer, ';', splite);
  for (unsigned int i = 0; i < splite.size(); ++i)
    {
      splite[i] = splite[i].substr(splite[i].find_first_not_of(' '));
      cmdPizza(splite[i]);
    }
}

int		Reception::getNbPizzas(std::string &nbr)
{
  std::stringstream		s;
  int				res;

  if (nbr.length() < 2 || nbr[0] != 'x')
    return -1;
  nbr = nbr.substr(1);
  s << nbr;
  s >> res;
  if (res == 0)
    return -1;
  return res;
}

void		Reception::cmdPizza(std::string &order)
{
  int				nb;
  std::vector<std::string>	parts;

  split(order, ' ', parts);
  if (parts.size() != 3)
    {
      std::cout << "Error: order must be of form: [PIZZA] [SIZE] [NBR]" << std::endl;
      return;
    }
  if (pizzas.find(parts[0]) == pizzas.end() ||
      pizzaSizes.find(parts[1]) == pizzaSizes.end())
    {
      std::cout << "Error: wrong pizza type or size" << std::endl;
      return;
    }
  if ((nb = getNbPizzas(parts[2])) == -1)
    {
      std::cout << "Error: wrong pizza number" << std::endl;
      return;
    }
  chooseKitchen(order);
}

void		Reception::chooseKitchen(const std::string &order)
{
  closeUnusedKitchens();
  if (kitchens.size() == 0)
    {
      kitchens.push_back(new Process<Kitchen>(0));
      if (kitchens.back()->fork(nbCooks, multiplier, replaceTime) == -2)
	std::exit(0);
      kitchens.back()->out() << order;
    }
  else
    for (unsigned int i=0; i < kitchens.size(); ++i)
      {
	Process<Kitchen> *tmp;

	tmp = kitchens[i];
	tmp->out() << order;
	return ;
      }
}

void		Reception::closeUnusedKitchens()
{
  std::string	res;
  for (unsigned int i=0; i < kitchens.size(); ++i)
    {
      kitchens[i]->out() << "dead?";
      kitchens[i]->in() >> res;
      if (res.empty() == true)
	{
	  delete kitchens[i];
	  kitchens.erase(kitchens.begin() + i);
	  i--;
	  std::cout << "BYE" << std::endl;
	}
	  std::cout << res << std::endl;
    }
}

std::map<std::string, TypePizza>	Reception::initPizzas()
{
  std::map<std::string, TypePizza>	res;
  
  res["regina"] = Regina;
  res["margarita"] = Margarita;
  res["americaine"] = Americaine;
  res["fantasia"] = Fantasia;

  return res;
}

std::map<std::string, TypePizza>	Reception::pizzas = initPizzas();
