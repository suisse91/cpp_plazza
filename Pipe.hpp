//
// pipe.hpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 13:56:46 2015 raphael elkaim
// Last update Fri Apr 17 12:13:57 2015 raphael elkaim
//

#ifndef PIPE_HPP_
# define PIPE_HPP_

#include <iostream>
#include <fstream>
#include <string>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

class Pipe
{
public:

  Pipe(const std::string &fname);
  virtual ~Pipe();

  bool	operator!();



protected:
  std::string	fifoName;
  int		isValid;
};

#endif
