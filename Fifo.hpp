//
// fifo.hpp for fifo in /home/elkaim_r/projects/cpp/cpp_plazza
// 
// Made by elkaim raphael
// Login   <elkaim_r@epitech.net>
// 
// Started on  Thu Apr 16 12:57:27 2015 elkaim raphael
// Last update Fri Apr 17 14:04:09 2015 raphael elkaim
//

#ifndef FIFO_HPP_
# define FIFO_HPP_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string>
#include "PipeOut.hpp"
#include "PipeIn.hpp"

class Fifo
{

private:

  std::string	fi;
  bool		valid;
  InPipe	in;
  OutPipe	out;

public:
  Fifo(const std::string &);
  ~Fifo();
  const std::string &name() const;
  bool operator!();
  void openRd();
  void openWr();
  bool changed();
  Fifo &operator>>(std::string &str);
  Fifo &operator<<(const std::string &str);
};

#endif
