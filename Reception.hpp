//
// Reception.hpp for reception in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Thu Apr  9 11:43:01 2015 arthur
// Last update Thu Apr 23 15:38:05 2015 raphael elkaim
//

#ifndef RECEPTION_HPP_
#define RECEPTION_HPP_

#include <map>
#include <string>
#include <vector>
#include "plazza.hpp"
#include "Kitchen.hpp"

class Kitchen;

class Reception
{
  float					multiplier;
  int					nbCooks;
  int					replaceTime;
  std::map<std::string, TaillePizza>	pizzaSizes;
  std::vector< Process <Kitchen> * >	kitchens;
  
public:
  static std::map<std::string,TypePizza> pizzas;
  
  Reception(float,int,int);
  ~Reception();

  void	start();

private:
  static std::map<std::string,TypePizza>	initPizzas();
  void	treatCmdLine(std::string&);
  void	split(const std::string&, char, std::vector<std::string>&);
  void	cmdPizza(std::string&);
  int	getNbPizzas(std::string&);
  void	chooseKitchen(const std::string &);
  void	closeUnusedKitchens();
};

#endif
