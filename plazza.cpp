//
// plazza.cpp for plazza in /home/elkaim_r/projects/cpp/cpp_plazza
// 
// Made by elkaim raphael
// Login   <elkaim_r@epitech.net>
// 
// Started on  Thu Apr 16 13:29:08 2015 elkaim raphael
// Last update Thu Apr 23 16:19:07 2015 raphael elkaim
//

#include <sstream>
#include "plazza.hpp"
#include "Kitchen.hpp"

std::string operator+(std::string str, int a)
{
  std::ostringstream in;

  in << a;
  str += in.str();
  return str;
}

void	*threadedCook(void *arg)
{
  Kitchen *parent;
  parent = static_cast<Kitchen *>(arg);
  Cook cooker(parent, parent->mult());
  cooker.run();
  return NULL;
}
