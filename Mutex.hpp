//
// mutex.hpp for mutex in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:57:59 2015 arthur
// Last update Thu Apr 23 12:24:44 2015 raphael elkaim
//

#ifndef MUTEX_HPP_
#define MUTEX_HPP_

#include <pthread.h>

class Mutex
{
  pthread_mutex_t	locker;

public:
  Mutex();
  ~Mutex();

  bool	lock();
  void	unlock();
};

#endif
