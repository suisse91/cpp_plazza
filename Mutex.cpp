//
// CondVar.hpp for condvar in /home/amstuta/rendu/cpp_plazza
//
// Made by arthur
// Login   <amstuta@epitech.net>
//
// Started on  Wed Apr 15 11:57:59 2015 arthur
// Last update Wed Apr 15 12:08:06 2015 arthur
//

#include "Mutex.hpp"

Mutex::Mutex()
{
}

Mutex::~Mutex()
{
}

bool	Mutex::lock()
{
  if (pthread_mutex_lock(&locker))
    return false;
  return true;
}

void	Mutex::unlock()
{
  pthread_mutex_unlock(&locker);
}
