//
// pipeOut.cpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 15:03:49 2015 raphael elkaim
// Last update Fri Apr 17 11:52:15 2015 raphael elkaim
//

#include "PipeIn.hpp"


InPipe::InPipe(const std::string &fname) :
  Pipe(fname),
  fifo()
{

}

InPipe::~InPipe()
{
  fifo.close();
}

bool	InPipe::open()
{
  fifo.open(fifoName.c_str(), std::ios_base::in);
  if (!fifo)
    return false;
  return true;
}

bool	InPipe::changed()
{
  if (fifo.peek() == EOF)
    {
      return false;
    }
  return true;
}

InPipe &InPipe::operator>>(std::string &str)
{
  std::getline(fifo, str);
  return *this;
}
