//
// pipe.cpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 14:05:31 2015 raphael elkaim
// Last update Thu Apr 16 13:06:58 2015 elkaim raphael
//

#include <iostream>
#include "Pipe.hpp"

Pipe::Pipe(const std::string &fname) :
  fifoName(fname)
{
}

Pipe::~Pipe()
{
}

bool	Pipe::operator!()
{
  if (access(fifoName.c_str(), F_OK))
    return false;
  return true;
}
