//
// pipe.hpp for plazza in /home/elkaim_r/project/cpp/cpp_plazza
// 
// Made by raphael elkaim
// Login   <elkaim_r@epitech.net>
// 
// Started on  Mon Apr 13 13:56:46 2015 raphael elkaim
// Last update Thu Apr 16 15:51:12 2015 elkaim raphael
//

#ifndef PIPEI_HPP_
# define PIPEI_HPP_

#include "Pipe.hpp"

class InPipe : public Pipe
{
public:

  InPipe(const std::string &);
  ~InPipe();
  bool	open();
  InPipe &operator>>(std::string &);
  bool	changed();


protected:
  std::fstream fifo;
};

#endif
